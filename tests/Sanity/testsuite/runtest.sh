#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/openhpi/Sanity/testsuite
#   Description: Running openhpi testsuite
#   Author: Radka Skvarilova <rskvaril@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="openhpi"
YUM=dnf
if rlIsRHEL '<8' ; then
   YUM=yum
fi
if rlIsRHEL 8 ; then
   RHEL8_OPTION="--enablerepo rhel-CRB-latest"
fi

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlFetchSrcForInstalled $PACKAGE
        rlRun "$YUM -y groupinstall 'Development Tools'" 
        rlRun "yum-builddep $RHEL8_OPTION -y *src.rpm"
        rlLogInfo "Sources fetched: `ls *src.rpm`"
        rlRun "rpm -q `basename *src.rpm .src.rpm`" 0 "Querying whether the sources fetched match the installed package"
        rlRun "rpmbuild --recompile *src.rpm" 0 "Recompiling the sources"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "pushd /root/rpmbuild/BUILD/openhpi-*/"
        rlRun "make check" 0 "Compile and run the tests"
        rlRun "popd"
    rlPhaseEnd
    
    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
