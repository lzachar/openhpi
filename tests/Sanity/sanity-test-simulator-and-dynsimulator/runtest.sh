#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/openhpi/Sanity/sanity-test-simulator-and-dynsimulator
#   Description: It use 2 simulators and one dyn simulator config
#   Author: Jan Scotka <jscotka@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="openhpi"
CONFIG="""
handler libsimulator {
        entity_root = \"{SYSTEM_CHASSIS,3}\"
        name = \"simulator\"
}

handler libsimulator {
        entity_root = \"{SYSTEM_CHASSIS,4}\"
        name = \"simulator2\"
}
handler libdyn_simulator {
        entity_root = \"{SYSTEM_CHASSIS,5}\"
        file = \"/etc/openhpi/simulation.data\"
        logflags = \"file stdout\"
        logfile = \"/var/log/dynsim\"
        logfile_max = \"5\"
}

"""

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlFileBackup /etc/openhpi/openhpi.conf
        rlRun "echo '$CONFIG' > /etc/openhpi/openhpi.conf"
        rlServiceStart openhpid
if rlIsRHEL 6; then
        rlRun "service openhpid status"
else
        rlRun "systemctl status -l openhpid"
fi
        sleep 5
    rlPhaseEnd

    rlPhaseStartTest

        rlRun "hpialarms |grep Chassis"
        rlRun "hpiel |egrep 'EventLogInfo for Chassis 3'"
        rlRun "hpiel |egrep 'EventLogInfo for Chassis 4'"
        rlRun "hpiinv |egrep -A 1 'SYSTEM_CHASSIS:4' |egrep 'Simulator HS DASD'"
        rlRun "hpipower -d -b 3"
        rlRun "hpitop | egrep -A 5 '{SYSTEM_CHASSIS,3}'  |grep 'Planar CPU area temperature sensor'"
        rlRun "hpiwdt | grep -A 3 'Chassis 3 has watchdog capability'"
        rlRun "hpievents | egrep 'rptentry\[[0-9]+\] tag: Chassis 4'"
        rlRun "hpionIBMblade |grep 'This program came with OpenHPI'"
        hpisettime -d 02/02/2015 -t 24:15:50
#       it sometimes fail from some unknow reason
        rlRun "timedatectl" #add according to failing ppc64
        rlRun "hpisettime -d 02/02/2015 -t 24:15:50 | egrep '.*2015-02-03 00:1'"
        rlRun "echo -e 'rpt\n\nq' | hpithres |grep 'Chassis 4'"

if rlIsRHEL ">6"; then 
        rlRun "hpidomain  | grep 'Serving Daemon on Host:'"
        rlRun "test `hpialarms |grep Chassis |wc -l` -eq 3"
        rlRun "hpifan | egrep 'Entity Path.*5'"
        rlRun "test `hpifan | egrep 'Entity Path.*[3-5]'|wc -l` -eq 3"
        rlRun "hpipower -d -b 3 | grep 'Chassis 3 3 is already powered off'"
        rlRun "hpipower -p -b 3 | grep 'Chassis 3 3 has been successfully powered on'"
        rlRun "hpisensor | egrep -A 5 '{SYSTEM_CHASSIS,5}{FAN,1}' |grep 'Fan Analog Control'"
        rlRun "hpigensimdata |egrep -A 4 'Chassis 4' |egrep 'SENSOR'"
        rlRun "test 5000 -lt `hpigensimdata |wc -l`"
        rlRun "hpireset -r -D 0 | egrep 'Chassis 4'"
        rlRun "hpitree |egrep  '{SYSTEM_CHASSIS,5}{FAN,1}' | grep 'Blower fan speed'"
        rlRun "hpixml |egrep  '{SYSTEM_CHASSIS,5}{FAN,1}'  |egrep '<Entity>.*</Entity>'"
        rlRun "timedatectl" #add according to failing ppc64
        rlRun "hpisettime -d 02/02/2015 -t 24:15:50 | egrep -A 2 'Current event' | egrep 'Read-Back-Check.*2015-02-03 00:15:50'"

fi

    rlPhaseEnd

    rlPhaseStartCleanup
       # rlFileRestore
        rlServiceRestore openhpid
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
