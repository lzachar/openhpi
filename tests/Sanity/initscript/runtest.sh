#!/bin/bash
# vim: dict=/usr/share/rhts-library/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/openhpi/Sanity/initscript
#   Description: Init script should meet LSB specifications
#   Author: Jan Scotka <jscotka@redhat.com>, Yulia Kopkova <ykopkova@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2009 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/share/rhts-library/rhtslib.sh

PACKAGE="openhpi"

SERVICE="openhpid"
SERVICE_CMD=systemctl

rlJournalStart
    
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "useradd testuserqa" 0 "Add test user"
        rlServiceStop $SERVICE
    rlPhaseEnd

    rlPhaseStartTest

        rlLog ">>>>>>>>> service start"
        rlRun "$SERVICE_CMD start $SERVICE" 0 " Service must start without problem"
        rlRun "$SERVICE_CMD status $SERVICE" 0 " Then Status command "
        rlRun "$SERVICE_CMD start $SERVICE" 0 " Already started service "
        rlRun "$SERVICE_CMD status $SERVICE" 0 " Again status command "

        rlLog ">>>>>>>>> service restart"
        rlRun "$SERVICE_CMD restart $SERVICE" 0 " Restarting of service"
        rlRun "$SERVICE_CMD status $SERVICE" 0 " Status command "

        rlLog ">>>>>>>>> service stop"
        rlRun "$SERVICE_CMD stop $SERVICE" 0 " Stopping service "
        rlRun "$SERVICE_CMD status $SERVICE" 3 " Status of stopped service "
        rlRun "$SERVICE_CMD stop $SERVICE" 0 " Stopping service again "
        rlRun "$SERVICE_CMD status $SERVICE" 3 " Status of stopped service "

        rlLog ">>>>>>>>> pid file"
        rlServiceStart $SERVICE
        rlAssertExists "/var/run/$SERVICE.pid" "Pid file /var/run/$SERVICE.pid must exist"
        rlRun "kill -9 `pidof $SERVICE`" 0 "Kill $SERVICE"
        rlRun "$SERVICE_CMD status $SERVICE" 3 " Status of stopped service "

        rlLog ">>>>>>>>> lock file"
        rlAssertNotExists "/var/lock/subsys/$SERVICE" "Lock file /var/lock/subsys/$SERVICE should not exist"
        rlRun "$SERVICE_CMD status $SERVICE" 3 " Status of stopped service "
        rlServiceStop $SERVICE

        rlLog ">>>>>>>>> insufficient rights"
        rlRun "$SERVICE_CMD start $SERVICE" 0 " Starting service for restarting under nonpriv user "
        rlRun "su testuserqa -c '$SERVICE_CMD restart $SERVICE'" 4 "Insufficient rights, restarting service under nonprivileged user must fail"

        rlLog ">>>>>>>>> operations"
        rlRun "$SERVICE_CMD start $SERVICE" 0 " Service have to implement start function "
        rlRun "$SERVICE_CMD restart $SERVICE" 0 " Service have to implement restart function "
        rlRun "$SERVICE_CMD status $SERVICE" 0 " Service have to implement status function "
        rlRun "$SERVICE_CMD condrestart $SERVICE" 0 " Service have to implement condrestart function "
        rlRun "$SERVICE_CMD try-restart $SERVICE" 0 " Service have to implement try-restart function "
        rlRun "$SERVICE_CMD force-reload $SERVICE" 0 " Service have to implement force-reload function "

    rlPhaseEnd

    rlPhaseStartCleanup
        rlServiceRestore $SERVICE
        rlRun "userdel -fr testuserqa" 0 "Remove test user"
    rlPhaseEnd

rlJournalPrintText
rlJournalEnd
