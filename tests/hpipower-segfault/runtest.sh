#!/bin/sh

# Copyright (c) 2006 Red Hat, Inc. All rights reserved. This copyrighted material 
# is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Author: Radek Biba <rbiba@redhat.com>

. /usr/bin/rhts-environment.sh

exec 5>&1 6>&2
exec >> ${OUTPUTFILE} 2>&1

result=FAIL

# Check if openhpid is already running
service openhpid status &> /dev/null && hpi_was_on=1 || hpi_was_on=0

# If not, start the openhpi daemon and a wait a few seconds
if [ $hpi_was_on -eq 0 ]; then
  service openhpid start
  sleep 5
fi

# Run hpipower with an index that surely doesn't exist,
# there's no apparent way to find the first unused index.
# Number 3 means displaying status.
# The wrong version will segfault here.

echo 3 | hpipower -b 9

if [ $? -eq 255 ]; then
  echo
  echo 'Success'
  result=PASS
fi


# Stop the daemon unless it was running before the test
if [ $hpi_was_on -eq 0 ]; then
  service openhpid stop
fi

exec 1>&5 2>&6
report_result $TEST $result
