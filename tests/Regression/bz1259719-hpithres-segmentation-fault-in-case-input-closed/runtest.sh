#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/openhpi/Regression/bz1259719-hpithres-segmentation-fault-in-case-input-closed
#   Description: Test for BZ#1259719 (hpithres, segmentation fault in case input closed)
#   Author: Radka Skvarilova <rskvaril@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="openhpi"
CONFIG="""
handler libsimulator {
        entity_root = \"{SYSTEM_CHASSIS,3}\"
        name = \"simulator\"
}

handler libsimulator {
        entity_root = \"{SYSTEM_CHASSIS,4}\"
        name = \"simulator2\"
}

"""

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlFileBackup /etc/openhpi/openhpi.conf
        rlFileBackup --clean /var/lib/openhpi/uid_map
        rlRun "echo '$CONFIG' > /etc/openhpi/openhpi.conf"
        rlServiceStart openhpid
        rlRun "systemctl status -l openhpid"
        sleep 5
    rlPhaseEnd

    rlPhaseStartTest
        rlRun  ' echo "rpt" | hpithres ' 0 "Test if there is no segmentation fault" 
     rlPhaseEnd

    rlPhaseStartCleanup
        rlServiceRestore openhpid
        rlFileRestore
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
