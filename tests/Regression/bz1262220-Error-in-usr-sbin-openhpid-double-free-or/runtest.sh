#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/openhpi/Regression/bz1262220-Error-in-usr-sbin-openhpid-double-free-or
#   Description: Test for BZ#1262220 (Error in `/usr/sbin/openhpid' double free or)
#   Author: Radka Skvarilova <rskvaril@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="openhpi"


rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlFileBackup "/etc/openhpi/openhpi.conf"
        rlFileBackup --clean "/var/lib/openhpi/uid_map"
        rlRun "sed -i 's/OPENHPI_UNCONFIGURED = \"YES\"/OPENHPI_UNCONFIGURED = \"NO\" /'  /etc/openhpi/openhpi.conf"
        rlRun "sed -i -e '/#handler libsysfs2hpi/,+3 s/^#//'  /etc/openhpi/openhpi.conf"
        
    rlPhaseEnd

    rlPhaseStartTest
        rlServiceStart openhpid
        sleep 5
        rlServiceStop openhpid
        rlRun "journalctl |grep  'double free or corruption' " 1  "Check the error in logs"



    rlPhaseEnd

    rlPhaseStartCleanup
        rlFileRestore
        rlServiceRestore openhpid
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
