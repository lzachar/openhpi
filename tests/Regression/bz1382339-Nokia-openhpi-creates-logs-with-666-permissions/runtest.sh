#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/openhpi/Regression/bz1382339-Nokia-openhpi-creates-logs-with-666-permissions
#   Description: Test for BZ#1382339 ([Nokia] openhpi creates logs with 666 permissions)
#   Author: Radka Skvarilova <rskvaril@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="openhpi"
CONFIG="""
handler libdyn_simulator {
        entity_root = \"{SYSTEM_CHASSIS,9}\"
        logflags = \"file stdout\"
        logfile = \"/var/log/dynsim\"
        logfile_max = \"5\"
}

"""
SEL=$(getenforce)

rlJournalStart
    rlPhaseStartSetup
        rlRun "setenforce 0"
        rlAssertRpm $PACKAGE
        rlFileBackup "/usr/lib/systemd/system/openhpid.service"
        rlFileBackup "/etc/openhpi/openhpi.conf"
        rlFileBackup --clean "/var/lib/openhpi/uid_map"
        rlRun "echo '$CONFIG' > /etc/openhpi/openhpi.conf"
        rlServiceStart openhpid
        cat /etc/openhpi/openhpi.conf
        rlRun "systemctl status -l openhpid" #check the setting output
        rlServiceStop openhpid
        sleep 5
    rlPhaseEnd
    
    
    rlPhaseStartTest "Default"
        UMASK=$((10#$(umask))) #umask in decimal number
        UMASK="${UMASK/7/6/}"
        PERM=$((666-$UMASK))   #count permissions for log file
        rlRun "rm -rf /var/log/dyn* " 0 "Removed old logs"
        rlRun "systemctl start openhpid" #start service
        sleep 5
        stat -c '%a %n' /var/log/dyn* | while read -r line ; do
            FILE=$(echo $line| cut -f 2 -d " ") 
            NUMBER=$(echo $line| cut -f 1 -d " ")
            rlAssertEquals "Check the permissons for $FILE" "$NUMBER" "$PERM"
        done
        rlRun "systemctl stop openhpid"
    rlPhaseEnd
    
    rlPhaseStartTest "Changed 0027"
        sed  -i '/\[Service\]/a UMask=027' /usr/lib/systemd/system/openhpid.service 
        rlRun "systemctl daemon-reload"
        UMASK=26 #umask in decimal number -7 write as 6 accoring to counting
        PERM=$((666-$UMASK))   #count permissions for log file
        rlRun "rm -rf /var/log/dyn* " 0  "Removed old logs"
        rlRun "systemctl start openhpid" #start service
        sleep 5
        stat -c '%a %n' /var/log/dyn* | while read -r line ; do #read permissons and file names
            FILE=$(echo $line| cut -f 2 -d " ") #file name
            NUMBER=$(echo $line| cut -f 1 -d " ") #permission number
            rlAssertEquals "Check the permissons for $FILE" "$NUMBER" "$PERM" 
        done
        rlRun "systemctl stop openhpid"
    rlPhaseEnd
    
    rlPhaseStartTest "Changed 0002"
        sed -i 's/UMask=027/UMask=002/' /usr/lib/systemd/system/openhpid.service 
        rlRun "systemctl daemon-reload"
        UMASK=2 #umask in decimal number -7 write as 6 accoring to counting
        PERM=$((666-$UMASK))   #count permissions for log file
        rlRun "rm -rf /var/log/dyn* " 0 "Removed old logs"
        rlRun "systemctl start openhpid" #start service
        sleep 5
        stat -c '%a %n' /var/log/dyn* | while read -r line ; do #read permissons and file names
            FILE=$(echo $line| cut -f 2 -d " ") #file name
            NUMBER=$(echo $line| cut -f 1 -d " ") #permission number
            rlAssertEquals "Check the permissons for $FILE" "$NUMBER" "$PERM" 
        done
        rlRun "systemctl stop openhpid"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlServiceRestore openhpid
        rlFileRestore
        rlRun "setenforce $SEL"
        rlRun "systemctl daemon-reload"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
