#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/openhpi/Regression/bz1259729-hpi-shell-Attempt-to-unlock-mutex-that-was-not
#   Description: Test for BZ#1259729 (hpi_shell - Attempt to unlock mutex that was not)
#   Author: Radka Skvarilova <rskvaril@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="openhpi"
CONFIG="""
handler libsimulator {
        entity_root = \"{SYSTEM_CHASSIS,3}\"
        name = \"simulator\"
}

handler libsimulator {
        entity_root = \"{SYSTEM_CHASSIS,4}\"
        name = \"simulator2\"
}

"""

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlFileBackup /etc/openhpi/openhpi.conf
        rlFileBackup --clean /var/lib/openhpi/uid_map
        rlRun "echo '$CONFIG' > /etc/openhpi/openhpi.conf"
        rlServiceStart openhpid
        rlRun "systemctl status -l openhpid"
        sleep 5
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        # try some random hpi commands
        export G_MESSAGES_DEBUG=all
        rlRun "hpisettime -t 12:00:00 -d 03/12/2015 -X" 0 "Just run few hpi commands" 
        rlRun "hpidomain" 0 "Just run few hpi commands" 
        rlRun "hpialarms" 0 "Just run few hpi commands" 
        rlRun "hpiel" 0 "Just run few hpi commands" 
                       
        # test for bug 1259729
        rlRun  "echo q |hpi_shell " 0,1 "Test if there is not problem with mutex lock (134 return code)" 
        rlServiceStop openhpid
        sleep 5
        rlServiceStart openhpid
        sleep 5
        rlRun  "echo q |hpi_shell " 0,1 "Test if there is not problem with mutex lock (134 return code)" 
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
        rlFileRestore
        rlServiceRestore openhpid
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
